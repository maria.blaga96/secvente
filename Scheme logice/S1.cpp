#include <stdio.h> // includerea bibliotecilor necesare rulării programelor


int main(void) // antetul funcției main
{ // de aici începe corpul functiei main;
int sum, dif, prod, number1, number2;
  printf("Introdu un numar intreg: ");  
	// reads and stores input
   	 scanf( "%d", &number1);
	printf("Introdu al doilea numar intreg: ");  
	// reads and stores input
   	 scanf( "%d", &number2);

	
 
// calculez suma;
sum = number1 + number2;
dif = number1 - number2;
prod = number1 * number2;


// g++ -o S1.exe S1.cpp

  printf("\nSuma numerelor este:%d\n", sum);
  printf("\nDiferenta numerelor este:%d\n", dif); 
  printf("\nProdusul numerelor este:%d\n", prod); 
 
  return 0; // functia main trebuie sa returneze mereu o valoare, iar 0 inseamna ca programul s-a finalizat cu succes
} //  se incheie corpul funcției;