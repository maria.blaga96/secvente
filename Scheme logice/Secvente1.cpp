#include <stdio.h> // includerea bibliotecilor necesare rulării programelor


int main() // antetul funcției main
{ // de aici începe corpul functiei main;

  printf("La\tmulti\tani!"); // afiseaza pe ecran textul dintre ghilimele cu tab orizontal
  return 0; // functia main trebuie sa returneze mereu o valoare, iar 0 inseamna ca programul s-a finalizat cu succes
} //  se incheie corpul funcției;